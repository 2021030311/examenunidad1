
package Modelo;

public class Docente {
    private int  nivel, horas;
    private String nombre, domicilio, numDocente;
    private float pagoBase;

    public Docente(){
        numDocente = "";
        nivel = 0;
        horas = 0;
        nombre = "";
        domicilio = "";
        pagoBase = 0;
    }
    
    public Docente(String numDocente, int nivel, int horas, String nombre, String domicilio, float pagoBase) {
        this.numDocente = numDocente;
        this.nivel = nivel;
        this.horas = horas;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.pagoBase = pagoBase;
    }
    
    public Docente(Docente docente) {
        this.numDocente = docente.numDocente;
        this.nivel = docente.nivel;
        this.horas = docente.horas;
        this.nombre = docente.nombre;
        this.domicilio = docente.domicilio;
        this.pagoBase = docente.pagoBase;
    }

    public String getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(String numDocente) {
        this.numDocente = numDocente;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }
    
    public float calcularPago(){
        if(this.nivel == 1){
            return this.horas * (this.pagoBase * (float)(1.3));
        }
        else if(this.nivel == 2){
            return this.horas * (this.pagoBase * (float)(1.5));
        }
        else if(this.nivel == 3){
            return this.horas * (this.pagoBase * (float)(2.0));
        }
        return 0;
    }
    
    public float calcularImpuesto(){
        return calcularPago() * (float)(.16);
    }
    
    public float calcularBono(int numHijos){

        if(numHijos >= 1 && numHijos <=2){
            return calcularPago() * (float)(.05);
        }
        else if(numHijos >= 3 && numHijos <= 5){
            return calcularPago() * (float)(.10);
        }
        else if(numHijos > 5){
            return calcularPago() * (float)(.20);
        }
         return 0;
    }
     
}
