
package Controlador;
import Modelo.Docente;
import Vista.dlgDocente;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JFrame;

public class Controlador implements ActionListener {
    
    private Docente doc;
    private dlgDocente vista;
    int hijos;
    
    public Controlador(Docente doc, dlgDocente vista){
        this.doc = doc;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
    }
    
    private void iniciarVista(){
        vista.setTitle(" == Docentes == ");
        vista.setSize(450, 400);
        vista.setVisible(true);
    }
    
    public void Limpiar(){
        vista.txtNombre.setText("");
        vista.txtNumDocente.setText("");
        vista.txtDomicilio.setText("");
        vista.txtPago.setText("");
        vista.txtHoras.setText("");
        vista.lHoras.setText(".");
        vista.LTotal.setText(".");
        vista.LImp.setText(".");
        vista.LBono.setText(".");
        vista.spHijos.setValue(0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnNuevo){
            vista.txtDomicilio.setEnabled(true);
            vista.txtHoras.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtNumDocente.setEnabled(true);
            vista.txtPago.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            vista.btnCerrar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.cbNivel.setEnabled(true);
            vista.spHijos.setEnabled(true);
        }
        
        if(e.getSource()==vista.btnLimpiar){
            Limpiar();
        }
        
        if(e.getSource() == vista.btnGuardar){
            doc.setNumDocente(vista.txtNumDocente.getText());
            doc.setDomicilio(vista.txtDomicilio.getText());
            doc.setNombre(vista.txtNombre.getText());
            
            doc.setHoras(Integer.parseInt(vista.txtHoras.getText()));
            doc.setPagoBase(Float.parseFloat(vista.txtPago.getText()));
            doc.setNivel(vista.cbNivel.getSelectedIndex()+1);
            hijos = Integer.parseInt(vista.spHijos.getValue().toString());
            if(vista.cbNivel.getSelectedIndex() == 0)
                doc.setNivel(1);
            else if(vista.cbNivel.getSelectedIndex() == 1)
                doc.setNivel(2);
            else if(vista.cbNivel.getSelectedIndex() == 2)
                doc.setNivel(3);

            Limpiar();
            }
        
        if(e.getSource() == vista.btnMostrar) {
            vista.txtNumDocente.setText(doc.getNumDocente());
            vista.txtNombre.setText(doc.getNombre());
            vista.txtDomicilio.setText(doc.getDomicilio());
            if(doc.getNivel() == 1)
                vista.cbNivel.setSelectedIndex(0);
            else if(doc.getNivel() == 2)
                vista.cbNivel.setSelectedIndex(1);
            else if(doc.getNivel() == 3);
                vista.cbNivel.setSelectedIndex(2);
                
            vista.txtPago.setText(Float.toString(doc.getPagoBase()));
            vista.txtHoras.setText(Integer.toString(doc.getHoras()));
            vista.spHijos.setValue(hijos);
            vista.lHoras.setText(Float.toString(doc.calcularPago()));
            vista.LBono.setText(Float.toString(doc.calcularBono(hijos)));
            vista.LImp.setText(Float.toString(doc.calcularImpuesto()));
            vista.LTotal.setText(Float.toString(doc.calcularPago() + Float.parseFloat(vista.LBono.getText()) - doc.calcularImpuesto()));
        }
        
        if(e.getSource() == vista.btnCancelar){
            Limpiar();
            hijos = 0;
            vista.txtDomicilio.setEnabled(false);
            vista.txtHoras.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtNumDocente.setEnabled(false);
            vista.txtPago.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            vista.btnCerrar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.cbNivel.setEnabled(false);
            vista.spHijos.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(vista, "¿Esta seguro que desea salir?", "Cerrar", JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
        
        

        
        }
        public static void main(String[] args) {
            Docente doc = new Docente();
            dlgDocente vista = new dlgDocente(new JFrame(), true);
        
            Controlador contra = new Controlador(doc, vista);
            contra.iniciarVista();
        }
    }
    

    

